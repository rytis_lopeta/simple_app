require "spec_helper"

describe Post do
  it "is invalid without title" do
    post = Post.new(title: nil, content: "turinys")
    expect(post.save).to be false
  end
  
  it "is invalid without content" do
    post = Post.new(title: "pavadinimas", content: nil)
    expect(post.save).to be false
  end
  
  it "is invalid when content is not between 5 and 30 characters" do
    post1 = Post.new(title: "pavadinimas", content: "t")
    post2 = Post.new(title: "pavadinimas",
      content: "turinys turinys turinys turinys turinys")
    expect(post1.save).to be false
    expect(post2.save).to be false
  end
end